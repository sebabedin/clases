import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.utils.multiclass import unique_labels
import numpy as np
import os
import PIL
import random
from google_images_download import google_images_download
from scipy.spatial.distance import cdist 
import lap

plt.rcParams['font.size'] = 20
plt.rcParams['figure.figsize'] = (10, 6)

# Funciones útiles


def mosaic(imgs_array, n_columns=6):
    """
    Función que toma un array de imágenes y devuelve una grilla o mosaico
    """
    dims = imgs_array.shape[1:3]

    width = int(n_columns * dims[1])
    n_rows = np.ceil(imgs_array.shape[0] / n_columns)
    height = int(n_rows * dims[0])

    if imgs_array.ndim == 3:
        mosaic = np.zeros((height, width))
    elif imgs_array.ndim == 4:
        mosaic = np.zeros((height, width, imgs_array.shape[3]))

    for k, img in enumerate(imgs_array):
        i = k // n_columns
        j = k % n_columns
        mosaic[i * dims[1]:(i + 1) * dims[1], j * dims[0]:(j + 1) * dims[0]] = img
    return mosaic


def cloud_to_grid(data2d, ncols=50):
    """
    Dada una nube de puntos en 2 dimensiones, devuelve el orden de esos puntos en una grilla 
    """
    ndata = data2d.shape[0]
    nrows = ndata // ncols
    xv, yv = np.meshgrid(np.linspace(0, 1, ncols), np.linspace(0, 1, nrows))
    grid = np.dstack((xv, yv)).reshape(-1, 2)
    cost = cdist(grid, data2d[:nrows * ncols], 'sqeuclidean')
    min_cost, row_assigns, col_assigns = lap.lapjv(np.copy(cost))
    grid_jv = grid[col_assigns]
    return grid_jv


def download_images(search_key, image_type='photo', limit=100):
    """
    Baja imágenes de google images 
    """
    response = google_images_download.googleimagesdownload()  # class instantiation
    arguments = {"keywords": search_key, "limit": limit, 'type': image_type}  # creating list of arguments
    response.download(arguments)  # passing the arguments to the function

def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data
    classes = unique_labels(y_true, y_pred)
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    ax = plt.gca()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    plt.tight_layout()
    return ax


def make_movie(images, out_dir, out_name):
    temp_dir = 'frames%06d'%int(1000000*random.random())
    os.system('mkdir %s'%temp_dir)
    for idx in tqdm(range(len(images))):
        PIL.Image.fromarray(images[idx], 'RGB').save('%s/frame%05d.png' % (temp_dir, idx))
    cmd = 'ffmpeg -i %s/frame%05d.png -c:v libx264 -pix_fmt yuv420p %s/%s.mp4' % (temp_dir, out_dir, out_name)
    print(cmd)
    os.system(cmd)
    os.system('rm -rf %s'%temp_dir)
