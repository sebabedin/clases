#%%

from keras.models import Sequential
from keras.layers import Dense
from keras.initializers import RandomNormal
import numpy as np
import matplotlib.pyplot as plt

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QSlider, QPushButton, QVBoxLayout

from vispy import app, scene

import time

#%%

scale = 1.0
scaleZ = 1.0
batch_size = 2 ** 12
image_size = 512
z_dim = 10
xy_dim = 2
dims = z_dim+xy_dim
image_channels = 1

layer_size = 32

initializer = RandomNormal(mean=0.0, stddev=1.0, seed=None)

model = Sequential()
model.add( Dense(layer_size, activation="tanh", kernel_initializer=initializer, use_bias=False, input_shape=(dims,) ) )

for i in range(12):
    model.add( Dense(layer_size, activation="tanh", kernel_initializer=initializer, use_bias=False ) )

model.add( Dense(image_channels, activation="sigmoid" ) )    

N = image_size
x,y = np.meshgrid(np.linspace(-1,1,N),np.linspace(-1,1,N))
x_ = x.reshape((x.size,1))
y_ = y.reshape((x.size,1))

def setRandomZ():
    global z
    
    z = np.random.normal( size=(z_dim,) )
    z = np.tile(z,N*N).reshape(N*N,-1)    

def synthImage():
    X = np.hstack( (z * scaleZ, x_ * scale, y_ * scale) )   
    
    y = model.predict(X * scale, batch_size=batch_size)
    y = y.reshape(N,N,image_channels)
    
    return np.squeeze(y)

setRandomZ()

#%%

canvas = scene.SceneCanvas(keys='interactive')
canvas.size = image_size, image_size

view = canvas.central_widget.add_view()
image = None
#view.camera = scene.PanZoomCamera(aspect=1)
#view.camera.flip = (0, 1, 0)
#view.camera.set_range()

#%%

class MainWindow(QWidget):

    def __init__(self):
        global image
        QWidget.__init__(self, None)
        
        image = scene.visuals.Image( synthImage() , interpolation='nearest',
                            parent=view.scene, cmap='grays')

        self.setMinimumSize(100, 100)

        canvas.show()

        self.initGui()
        self.show()

    def initGui(self):
        self.scaleSlider = QSlider(self)
        self.zSlider = QSlider(self)
        self.lblScaleXY = QLabel("ScaleXY", self)
        self.lblScaleZ = QLabel("ScaleZ", self)
        
        self.scaleSlider.valueChanged.connect(self.slider_changed)
        self.scaleSlider.setMinimum(-1000)
        self.scaleSlider.setMaximum(1000)
        self.scaleSlider.setOrientation( Qt.Horizontal )
        self.scaleSlider.setValue(100)
        
        self.zSlider.valueChanged.connect(self.slider_changed)
        self.zSlider.setMinimum(-1000)
        self.zSlider.setMaximum(1000)
        self.zSlider.setOrientation( Qt.Horizontal )
        self.zSlider.setValue(100)
        
        self.btnNewZ = QPushButton("Random Z", self)
        self.btnNewZ.clicked[bool].connect(self.btnNewZ_clicked)
        
        vlayout = QVBoxLayout()
        vlayout.addWidget(self.lblScaleXY, 0)
        vlayout.addWidget(self.scaleSlider, 0)
        vlayout.addWidget(self.lblScaleZ, 0)
        vlayout.addWidget(self.zSlider, 0)
        vlayout.addWidget(self.btnNewZ, 0)
        self.setLayout(vlayout)

    def btnNewZ_clicked(self):
        setRandomZ()
        self.slider_changed()
        
    def slider_changed(self):
        global scale, scaleZ
        
        scale = self.scaleSlider.value()  / 100
        scaleZ = self.zSlider.value() / 100
        print("ScaleZ: {}, ScaleXY: {}".format(scaleZ, scale))
        
        tic = time.clock()
        image.set_data( synthImage() )
        canvas.update()
        print("time: {}".format( time.clock() - tic ))


if __name__ == '__main__':
    app.create()
    m = MainWindow()
    app.run()
