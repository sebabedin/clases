import numpy as np
from PyQt5.QtWidgets import QApplication, QWidget, QSlider, QVBoxLayout
from vispy import app, scene

canvas = scene.SceneCanvas(keys='interactive')
canvas.size = 600, 600
view = canvas.central_widget.add_view()
X = np.random.random((400, 400)) * 50 + 128
image = scene.visuals.Image(X.astype(np.ubyte), interpolation='nearest',
                            parent=view.scene, cmap='grays')
view.camera = scene.PanZoomCamera(aspect=1)
view.camera.flip = (0, 1, 0)
view.camera.set_range()


class MainWindow(QWidget):

    def __init__(self):
        QWidget.__init__(self, None)

        self.setMinimumSize(100, 100)

        self.theSlider = QSlider(self)
        self.theSlider.valueChanged.connect(self.slider_changed)
        self.theSlider.setMinimum(0)
        self.theSlider.setMaximum(100)
        self.theSlider.setValue(50)
        self.theSlider.setTickPosition(QSlider.TicksBelow)
        self.theSlider.setTickInterval(5)

        canvas.show()

        vlayout = QVBoxLayout()
        vlayout.addWidget(self.theSlider, 0)
        self.show()

    def slider_changed(self):
        val = self.theSlider.value() / 100 * 255
        print(val)
        X = np.random.random((400, 400)) * 50 + val
        image.set_data(X.astype(np.ubyte))
        canvas.update()


if __name__ == '__main__':
    app.create()
    m = MainWindow()
    app.run()
