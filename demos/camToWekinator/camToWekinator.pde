import processing.video.*;
import oscP5.*;
import netP5.*;

Capture cam;


/*
  Estas dos variables modifican el shape (la forma, la dimensionalidad) de la salida.
  
  En este caso la resolución de la imagen de salida.
  
  20x20 2000 ejemplos 20 neuronas 1 capa 5 min
*/
int targetWidth = 20;
int targetHeight = 20; 


int hop;

boolean originalCamera = false;


color pixs[] = new color[targetWidth*targetHeight];

PImage imgOutput;


OscP5 oscP5;
NetAddress dest;
boolean sendOSC = true;
int wekinatorReceivePort = 6448;
int processingReceivePort = 12001; //12000

/*
  Cuando empiece el programa arrojará un output en la consola.
  
  Leer ese output y elegir una de las cámaras.
  
  Yo elegí una de 160x120
*/

void setup() {
  size(400, 400);

  String[] cameras = Capture.list();
  
  if (cameras.length == 0) {
    println("There are no cameras available for capture.");
    exit();
  } else {
    
    // 1. Leer este output y elegir una cámara en 2.
    
    println("Available cameras:");
    for (int i = 0; i < cameras.length; i++) {
      println(str(i) + ": " + cameras[i]);
    }
    
    // 2. Cambiar este índice al que quieran usar.  
    int indiceCam = 53;//13;
    
    cam = new Capture(this, cameras[indiceCam]);
    cam.start();
    
    imgOutput = createImage(targetWidth, targetHeight, RGB);
    
    
    oscP5 = new OscP5(this,processingReceivePort);
    dest = new NetAddress("127.0.0.1",wekinatorReceivePort);
  }      
}

void draw() {
  //background(0);
  int screenUnitWidth = width/targetWidth;
  int screenUnitHeight = height/targetHeight;
  int unitWidth = cam.width/targetWidth;
  int unitHeight = cam.height/targetHeight;
  
  if (cam.available() == true) {
    cam.read();
    
    cam.loadPixels();
    
    float target = (targetWidth * targetHeight);
    float camDims = (cam.width * cam.height);
    hop = floor(camDims / target);
    
    for ( int i = 0 ; i < targetWidth * targetHeight ; i++ ) {
      pixs[i] = cam.pixels[i*hop];
    }
    
    imgOutput.loadPixels();
    for ( int y = 0 ; y < imgOutput.height ; y+= 1 ) {
      for ( int x = 0 ; x < imgOutput.width ; x+= 1 ) {
        //pixels[y*width+x]
        color pix = cam.pixels[y*unitHeight*cam.width+x*unitWidth];// get(x*unitWidth,y*unitHeight);
        imgOutput.pixels[y*imgOutput.width+x] = pix;//set(x,y, pix);
        
        rect(x*screenUnitWidth,
             y*screenUnitHeight,
             screenUnitWidth,
             screenUnitHeight);
      }
    }
    imgOutput.updatePixels();
  }
  
  if ( originalCamera ) {
    image(cam, 0, 0);
  } else {
    //image(imgOutput,0,0);
    
    for ( int y = 0 ; y < imgOutput.height ; y+= 1 ) {
      for ( int x = 0 ; x < imgOutput.width ; x+= 1 ) {
        fill(imgOutput.pixels[y*imgOutput.width+x]);//.get(x,y));
        rect(x*screenUnitWidth,y*screenUnitHeight,screenUnitWidth,screenUnitHeight);
      }
    }
  }
  
  if ( sendOSC ) {
    enviarOSC();
  }
}

void keyPressed() 
{
  if ( key == 'c' ) {
    originalCamera = !originalCamera;
  }
  if ( key == 'h' ) {
    //println("Target is: " + str(target));
    //println("CamDims is: " + str(camDims));
    println("Hop is: " + str(hop));
  }
  if ( key == 's' ) {
    sendOSC = !sendOSC;
  }
}

void enviarOSC() {
  OscMessage msg = new OscMessage("/wek/inputs");
  for ( int i = 0; i < imgOutput.width * imgOutput.height; i++ ) {
    msg.add((float)imgOutput.pixels[i] );
  }
  
  oscP5.send(msg, dest);
}
